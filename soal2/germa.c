#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>

static const char *logPath = "/home/reo/Documents/4/logmucatatsini.txt";
static const char *dirpath = "/home/reo/Documents/4/nanaxgerma/src_data";
static const char *magicWord = "bypass";

void appendLog(const char *status, const char *command, const char *description, const char* add)
{
    time_t now;
    struct tm *tm_info;
    char timestamp[20];

    time(&now);
    tm_info = localtime(&now);

    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", tm_info);

    FILE *logFile = fopen(logPath, "a");
    if (logFile != NULL)
    {
    	if(strcmp(command,"MKDIR")==0)
    	{
            fprintf(logFile, "%s::%s::%s::reo-Create directory %s\n", status, timestamp, command, strrchr(description,'/'));
            fclose(logFile);
        }
        if(strcmp(command,"MV")==0)
    	{
            fprintf(logFile, "%s::%s::%s::reo-Rename from %s to %s\n", status, timestamp, command, strrchr(description,'/'), strrchr(add,'/'));
            fclose(logFile);
        }
        if(strcmp(command,"RM")==0)
    	{
            fprintf(logFile, "%s::%s::%s::reo-Remove file %s\n", status, timestamp, command, strrchr(description,'/'));
            fclose(logFile);
        }
        if(strcmp(command,"RMDIR")==0)
    	{
            fprintf(logFile, "%s::%s::%s::reo-Remove directory %s\n", status, timestamp, command, strrchr(description,'/'));
            fclose(logFile);
        }
    }
}

static int isRestricted(const char *path)
{
    // Check if the path contains the word "restricted"
    return strstr(path, "restricted") != NULL;
}

static int checkRestricted(const char *path)
{
    char fullpath[1000];
    const char *token;
    int flag=0;

    strcpy(fullpath, path);

    // Tokenize the path and check each component
    token = strtok(fullpath, "/");
    while (token != NULL)
    {
        
        if (isRestricted(token))
            flag = 1;
            
        // Check if the magic word "bypass" is present in the path
        if (strstr(token, magicWord) != NULL)
            flag = 0; 
        
        token = strtok(NULL, "/");
    }

    return flag;
}

static int checkMagic(const char *path)
{
    char fullpath[1000];
    const char *token;
    int flag=0;

    strcpy(fullpath, path);

    // Tokenize the path and check each component
    token = strtok(fullpath, "/");
    const char *lastToken = NULL;
    while (token != NULL)
    {
        lastToken = token;
        token = strtok(NULL, "/");
    }
    
    if(lastToken != NULL && strstr(lastToken, magicWord)!=NULL)
        flag = 1;

    return flag;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    // Check if the path or any parent directories are restricted
    if (checkRestricted(fpath))
    {
        appendLog("FAILED", "MKDIR", path,"");
        return -EACCES; // Access denied
    }
    
    int res = mkdir(fpath, mode);

    if (res == -1)
    {
        appendLog("FAILED", "MKDIR", path,"");
        return -errno;
    }
    
    appendLog("SUCCESS", "MKDIR", path,"");
    return 0;
}

static int xmp_unlink(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Check if the path or any parent directories are restricted
    if (checkRestricted(fpath))
    {
        appendLog("FAILED", "RM", path, "");
        return -EACCES; // Access denied
    }
    
    int res = unlink(fpath);
    if (res == -1)
    {
        appendLog("FAILED", "RM", path, "");
        return -errno;
    }
    
    appendLog("SUCCESS", "RM", path, "");
    return 0;
}

static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Check if the path or any parent directories are restricted
    if (checkRestricted(fpath))
    {
        appendLog("FAILED", "RMDIR", path, "");
        return -EACCES; // Access denied
    }
    
    int res = rmdir(fpath);
    if (res == -1)
    {
        appendLog("FAILED", "RMDIR", path, "");
        return -errno;
    }
    
    appendLog("SUCCESS", "RMDIR", path, "");
    return 0;
}

static int xmp_rename(const char *oldpath, const char *newpath)
{
    char f_oldpath[1000];
    char f_newpath[1000];
    sprintf(f_oldpath, "%s%s", dirpath, oldpath);
    sprintf(f_newpath, "%s%s", dirpath, newpath);

    // Check if the old path or any parent directories are restricted
    if (checkRestricted(f_oldpath))
         if (!checkMagic(f_newpath))
         {
             appendLog("FAILED", "MV", oldpath, newpath);
             return -EACCES; // Access denied
         }
         
    int res = rename(f_oldpath, f_newpath);
    if (res == -1)
    {
        appendLog("FAILED", "MV", oldpath, newpath);
        return -errno;
    }
    
    appendLog("SUCCESS", "MV", oldpath, newpath);
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

