#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

void downloadDataset() {
  system("kaggle datasets download -d bryanb/fifa-player-stats-database");
}

void unzipFile() {
  char* unzipCmd[] = {"/usr/bin/unzip", "fifa-player-stats-database.zip", NULL};
  execvp("/usr/bin/unzip", unzipCmd);
  exit(1);
}

void analyzeData() {
  system("awk -F , '$3 <= 25 && $8 >= 85 && $9 != \"Manchester City\" {printf(\"%-20s %-20s %-20s %-5s %-20s %-5s %-10s\\n\", $2, $9, $5, $3, $7, $8, $4); print \"------------------\"} END {print \"\"}' FIFA23_official_data.csv");
}

int main() {
  pid_t pid;
  int status;

  pid = fork();

  if (pid == 0) {
    printf("Downloading dataset...\n");
    downloadDataset();
    printf("Extracting dataset...\n");
    unzipFile();
  } else {
    wait(&status);
    printf("Analyzing data...\n");
    analyzeData();
  }

  return 0;
}
