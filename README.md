<div align = center>

# **Praktikum Sistem Operasi - Modul 4**

# **Authors**
| Name                  | NRP          | Github Address                                     | 
| :--------             | :-------     | :-------------------------                         |
| `Adam Haidar Azizi`   | `5025211114` | [@HADAIZI](https://github.com/HADAIZI)             |
| `Afiq Fawwaz Haidar`  | `5025211246` | [@AfiqHaidar](https://github.com/AfiqHaidar)       |
| `Alfan Lukeyan Rizki` | `5025211046` | [@AlfanLukeyan](https://github.com/AlfanLukeyan)   |

</div>

# **Daftar Isi**
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)
<br/>
<div align=justify>

## **Soal 1**

Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline `#YBBA` `(#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪)` sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.
<ol type="a">
<li>

Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama `storage.c`. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

```c
kaggle datasets download -d bryanb/fifa-player-stats-database
```

Setelah berhasil men-download dalam format `.zip`, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file `storage.c` untuk semua pengerjaannya. 
</li>
<li>

Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama `FIFA23_official_data.csv` dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain `Manchester City`. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file `storage.c` untuk analisis ini.
</li>
<li>

Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah `Docker Container` agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah `Dockerfile` yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.

Setelah `Dockerfile` berhasil dibuat, langkah selanjutnya adalah membuat `Docker Image`. Gunakan` Docker CL`I untuk mem-build image dari `Dockerfile` kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan `Docker Container` dan memeriksa output-nya.
</li>
<li>

Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?

Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish `Docker Image` sistem ke `Docker Hub`, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

</li>
<li>

Berita tagline `#YBBA (#YangBiruBiruAja)` semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan `Docker Compose` dengan `instance sebanyak 5`. Buat folder terpisah bernama `Barcelona` dan `Napoli` dan jalankan `Docker Compose` di sana.
</li>
</ol>

**Catatan:**

-   Cantumkan file `hubdocker.txt` yang berisi `URL Docker Hub kalian` (public).
-   Perhatikan `port`  pada masing-masing `instance`.


### **Penyelesaian**

#### Download, unzip, analysis data
```c
void downloadDataset() {
  system("kaggle datasets download -d bryanb/fifa-player-stats-database");
}
```

1. **downloadDataset()**: Fungsi ini menggunakan fungsi `system()` untuk menjalankan perintah "kaggle datasets download -d bryanb/fifa-player-stats-database". Fungsi ini bertugas untuk mengunduh dataset dari platform Kaggle.

```c
void unzipFile() {
  char* unzipCmd[] = {"/usr/bin/unzip", "fifa-player-stats-database.zip", NULL};
  execvp("/usr/bin/unzip", unzipCmd);
  exit(1);
}
```

2. **unzipFile()**: Fungsi ini menggunakan fungsi `execvp()` untuk menjalankan perintah "/usr/bin/unzip fifa-player-stats-database.zip". Fungsi ini bertugas untuk mengekstrak dataset yang telah diunduh.

```c
void analyzeData() {
  system("awk -F , '$3 <= 25 && $8 >= 85 && $9 != \"Manchester City\" {printf(\"%-20s %-20s %-20s %-5s %-20s %-5s %-10s\\n\", $2, $9, $5, $3, $7, $8, $4); print \"------------------\"} END {print \"\"}' FIFA23_official_data.csv");
}
```


3. **analyzeData()**: Fungsi ini menggunakan fungsi `system()` untuk menjalankan perintah "awk -F , '$3 <= 25 && $8 >= 85 && $9 != "Manchester City" {printf("%-20s %-20s %-20s %-5s %-20s %-5s %-10s\n", $2, $9, $5, $3, $7, $8, $4); print "------------------"} END {print ""}' FIFA23_official_data.csv". Fungsi ini menganalisis data dari file "FIFA23_official_data.csv".


Pada fungsi `main()`, program ini melakukan proses sebagai berikut:
```c
int main() {
  pid_t pid;
  int status;

  pid = fork();

  if (pid == 0) {
    printf("Downloading dataset...\n");
    downloadDataset();
    printf("Extracting dataset...\n");
    unzipFile();
  } else {
    wait(&status);
    printf("Analyzing data...\n");
    analyzeData();
  }

  return 0;
}

```

- Membuat proses baru menggunakan `fork()` dan menyimpan ID proses tersebut di dalam variabel `pid`.
- Jika `pid` bernilai 0, maka program berada dalam konteks proses anak (child process) yang bertugas untuk mendownload dataset dan mengekstraknya.
  - Menampilkan pesan "Downloading dataset..." menggunakan `printf()`.
  - Memanggil fungsi `downloadDataset()` untuk mengunduh dataset.
  - Menampilkan pesan "Extracting dataset..." menggunakan `printf()`.
  - Memanggil fungsi `unzipFile()` untuk mengekstrak dataset.
- Jika `pid` bukan 0, maka program berada dalam konteks proses induk (parent process) yang menunggu proses anak selesai.
  - Menunggu proses anak selesai menggunakan `wait(&status)`.
  - Menampilkan pesan "Analyzing data..." menggunakan `printf()`.
  - Memanggil fungsi `analyzeData()` untuk menganalisis data.
  
#### Build & run images


Dockerfile ini digunakan untuk membuat image Docker yang menjalankan program `storage.c` dengan menggunakan Kaggle API. Berikut adalah penjelasan singkat mengenai langkah-langkah dalam Dockerfile ini:

```bash
FROM ubuntu:latest
```
Menggunakan base image Ubuntu versi terbaru.

```bash
RUN apt-get update && apt-get install -y unzip gawk gcc python3 python3-dev python3-pip && rm -rf /var/lib/apt/lists/*
```
Mengupdate paket-paket pada image Ubuntu dan menginstal beberapa paket yang dibutuhkan.

```bash
ADD storage.c / 
ADD kaggle.json /root/.kaggle/
RUN chmod 600 /root/.kaggle/kaggle.json
```
Menambahkan file `storage.c` ke dalam image dan menambahkan file `kaggle.json` ke direktori `/root/.kaggle/` pada image.

```bash
RUN gcc storage.c -o storage
```
Mengompilasi file `storage.c` menjadi file eksekusi `storage`.

```bash
RUN pip3 install kaggle
```
Menginstal paket `kaggle` menggunakan pip3.

```bash
CMD ["./storage"]
```
Menjalankan program `storage` saat container berjalan.

Dockerfile ini dapat digunakan untuk membangun image Docker dengan perintah `docker build -t <nama_image>:<tag> .`. Image tersebut dapat digunakan untuk membuat container yang menjalankan program `storage.c` dengan menggunakan perintah `docker run <nama_image>:<tag>`.


#### Push ke Docker

Untuk mem-publish Docker Image ke Docker Hub, kamu dapat mengikuti langkah-langkah berikut:

1. Pastikan kamu telah membuat akun Docker Hub di https://hub.docker.com/. Jika belum, buat akun terlebih dahulu.

2. Buka terminal dan pastikan kamu sudah login ke akun Docker Hub dengan menjalankan perintah berikut:

```bash
docker login
```

Ikuti instruksi untuk memasukkan username dan password Docker Hub kamu.

3. Setelah berhasil login, gunakan perintah berikut untuk mem-tag Docker Image yang telah kamu buat:

```bash
docker tag nama_image:tag username/storage-app:tag
```

Gantilah "nama_image:tag" dengan nama dan tag dari Docker Image yang ingin kamu publish, dan "username" dengan username Docker Hub kamu. Juga, gantilah "storage-app:tag" dengan nama dan tag yang ingin kamu berikan pada image yang dipublish di Docker Hub.

Contoh:

```bash
docker tag ybba:v1.0 myusername/storage-app:v1.0
```

4. Setelah melakukan tagging, gunakan perintah berikut untuk mem-publish Docker Image ke Docker Hub:

```bash
docker push username/storage-app:tag
```

Gantilah "username/storage-app:tag" dengan nama dan tag yang telah kamu berikan pada langkah sebelumnya.

Contoh:

```shell
docker push myusername/storage-app:v1.0
```

5. Docker akan mulai mengunggah Docker Image ke Docker Hub. Proses ini dapat memakan waktu tergantung pada ukuran image dan kecepatan internet kamu. Setelah selesai, Docker Image akan tersedia secara publik di repositori Docker Hub dengan URL yang telah kamu sebutkan sebelumnya:

```
https://hub.docker.com/r/{Username}/storage-app
```

#### docker-compose

1. Buat Folder Terpisah
Buat dua folder terpisah dengan nama "Barcelona" dan "Napoli". Folder ini akan digunakan untuk mengatur konfigurasi layanan Docker Compose yang akan dijalankan.

2. Buat File `docker-compose.yml`
Buat file dengan nama `docker-compose.yml` pada direktori utama proyek. File ini akan berisi konfigurasi Docker Compose untuk layanan "Barcelona" dan "Napoli" yang akan dijalankan dengan skala 5 instance.

```yaml
version: '3'

services:
  barcelona:
    build:
      context: ./Barcelona
      dockerfile: Dockerfile
    deploy:
      replicas: 5
    ports:
      - 8081-8085:8080

  napoli:
    build:
      context: ./Napoli
      dockerfile: Dockerfile
    deploy:
      replicas: 5
    ports:
      - 8091-8095:8080
```

3. Konfigurasi Layanan "Barcelona"
Dalam blok konfigurasi `services`, terdapat konfigurasi untuk layanan "Barcelona". Berikut adalah penjelasan konfigurasi yang ada:

- `build`: Mengatur konfigurasi build untuk layanan "Barcelona". Menentukan `context` (direktori) di mana file `Dockerfile` untuk layanan tersebut berada.
- `deploy`: Menentukan konfigurasi untuk deployment layanan. Di sini, kita mengatur `replicas` menjadi 5, yang berarti akan menjalankan 5 instance dari layanan ini.
- `ports`: Meneruskan port dari host ke port layanan Docker. Dalam contoh ini, kita meneruskan port 8081-8085 dari host ke port 8080 di dalam kontainer Docker.

4. Konfigurasi Layanan "Napoli"
Blok konfigurasi untuk layanan "Napoli" mirip dengan konfigurasi layanan "Barcelona". Hanya perlu mengganti nama layanan, konteks, dan port yang sesuai.

5. Jalankan Docker Compose
Buka terminal atau command prompt, pastikan kamu berada di direktori yang berisi file `docker-compose.yml`. Jalankan perintah berikut untuk menjalankan layanan dengan skala yang telah ditentukan:

```bash
docker-compose up -d
```

Perintah ini akan menjalankan services "Barcelona" dan "Napoli" dengan masing-masing 5 instance.

atau seperti ini
```bash
docker-compose run <nama_service>
```
untuk menjalanakan salah satu service



</div>
<br/>
<div align=justify>

## **Soal 2**
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di [germa.dev](https://www.germa.dev/) sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file [.zip](https://drive.google.com/file/d/1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i/view?usp=sharing) yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
-   Apabila terdapat file yang mengandung kata `restricted`, maka file tersebut tidak dapat di-rename ataupun dihapus.
-   Apabila terdapat folder yang mengandung kata `restricted`, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.

**Contoh:**

    /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.

**Case invalid untuk bypass:**

    /jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
    /jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
    /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt

**Case valid untuk bypass:**

    /jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
    /jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
    /jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
    /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama `germa.c`, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:

<ol type="a">
<li>

Buatlah folder `productMagang` pada folder `/src_data/germa/products/restricted_list/`. Kemudian, buatlah folder `projectMagang` pada `/src_data/germa/projects/restricted_list/`. Akan tetapi, hal tersebut akan gagal.
</li>
<li>

Ubahlah nama folder dari restricted_list pada folder `/src_data/germa/projects/restricted_list/` menjadi `/src_data/germa/projects/bypass_list/`. Kemudian, buat folder `projectMagang` di dalamnya.
</li>
<li>

Karena folder `projects` menjadi dapat diakses, ubahlah folder `filePenting` di dalam folder `projects` menjadi `restrictedFilePenting` agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
</li>
<li>

Terakhir, coba kamu hapus semua `fileLama` di dalam folder `restrictedFileLama`. Jangan lupa untuk menambahkan kata `sihir` pada folder tersebut agar folder tersebut dapat terhapus
</li>
<li>

Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file `logmucatatsini.txt` di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 
Adapun format logging yang harus kamu buat adalah sebagai berikut. 

```
[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]
```
Dengan keterangan sebagai berikut.
<ul>
<li>

`STATUS`: SUCCESS atau FAILED.
</li>
<li>

`dd`: 2 digit tanggal.
</li>
<li>

`MM`: 2 digit bulan.
</li>
<li>

`yyyy`: 4 digit tahun.
</li>
<li>

`HH`: 24-hour clock hour, with a leading 0 (e.g. 22).
</li>
<li>

`mm`: 2 digit menit.
</li>
<li>

`ss`: 2 digit detik.
</li>
<li>

`CMD`: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
</li>
<li>

`DESC`: keterangan action, yaitu:
<ul>
<li>
[User]-Create directory x.
</li>
<li>
[User]-Rename from x to y
</li>
<li>
[User]-Remove directory x.
</li>
<li>
[User]-Remove file x.
</li>
</ul>
</li>
</ol>

**Contoh:**
```
SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y
```

**Catatan:**

-   Keterangan untuk file .zip.
    -   Folder `src_data` adalah `folder source FUSE.`
    -   Folder `dest_data` adalah `folder destination FUSE`.

### **Penyelesaian**

```c
static const char *logPath = "/home/reo/Documents/4/logmucatatsini.txt";
static const char *dirpath = "/home/reo/Documents/4/nanaxgerma/src_data";
static const char *magicWord = "bypass";
```
Variabel logPath menyimpan jalur lengkap ke file log yang akan digunakan untuk mencatat kegiatan sistem berkas. Variabel dirpath menyimpan jalur lengkap ke direktori yang akan digunakan sebagai titik awal sistem berkas. Variabel magicWord berisi kata kunci khusus yang akan digunakan dalam operasi tertentu.

```c
void appendLog(const char *status, const char *command, const char *description, const char* add)
{
    time_t now;
    struct tm *tm_info;
    char timestamp[20];

    time(&now);
    tm_info = localtime(&now);

    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", tm_info);

    FILE *logFile = fopen(logPath, "a");
    if (logFile != NULL)
    {
    	if(strcmp(command,"MKDIR")==0)
    	{
            fprintf(logFile, "%s::%s::%s::reo-Create directory %s\n", status, timestamp, command, strrchr(description,'/'));
            fclose(logFile);
        }
        if(strcmp(command,"MV")==0)
    	{
            fprintf(logFile, "%s::%s::%s::reo-Rename from %s to %s\n", status, timestamp, command, strrchr(description,'/'), strrchr(add,'/'));
            fclose(logFile);
        }
        if(strcmp(command,"RM")==0)
    	{
            fprintf(logFile, "%s::%s::%s::reo-Remove file %s\n", status, timestamp, command, strrchr(description,'/'));
            fclose(logFile);
        }
        if(strcmp(command,"RMDIR")==0)
    	{
            fprintf(logFile, "%s::%s::%s::reo-Remove directory %s\n", status, timestamp, command, strrchr(description,'/'));
            fclose(logFile);
        }
    }
}
```
Fungsi ini digunakan untuk menambahkan entri log ke file log dengan format tertentu. Fungsi ini akan mencatat status, waktu, perintah, dan deskripsi dari setiap operasi yang dilakukan pada sistem berkas.

- Menerima argumen status, command, description, dan add yang akan digunakan untuk mencatat log.
- Mendeklarasikan variabel fp sebagai pointer ke file log dan timestamp untuk menyimpan waktu saat ini.
- Menggunakan fungsi fopen untuk membuka file log dalam mode append. Jika gagal, mencetak pesan kesalahan.
- Membuat format log menggunakan variabel status, timestamp, command, description, dan add. Menggunakan fungsi fprintf untuk menulis format log ke file log.
- Menutup file log dengan menggunakan fungsi fclose.


```c
static int isRestricted(const char *path)
{
    // Check if the path contains the word "restricted"
    return strstr(path, "restricted") != NULL;
}
```
Fungsi ini digunakan untuk memeriksa apakah suatu jalur (path) terlarang atau mengandung kata kunci yang harus dibatasi aksesnya.
- Menerima argumen path yang merupakan jalur file atau direktori yang akan diperiksa.
- Mengecek apakah jalur (path) mengandung kata kunci yang harus dibatasi aksesnya. Jika ya, mengembalikan nilai 1 (terlarang), jika tidak, mengembalikan nilai 0 (tidak terlarang).


```c

static int checkRestricted(const char *path)
{
    char fullpath[1000];
    const char *token;
    int flag=0;

    strcpy(fullpath, path);

    // Tokenize the path and check each component
    token = strtok(fullpath, "/");
    while (token != NULL)
    {
        
        if (isRestricted(token))
            flag = 1;
            
        // Check if the magic word "bypass" is present in the path
        if (strstr(token, magicWord) != NULL)
            flag = 0; 
        
        token = strtok(NULL, "/");
    }

    return flag;
}
```
Fungsi ini digunakan untuk memeriksa apakah jalur (path) atau direktori induk dari jalur tersebut terlarang.
- Menerima argumen path yang merupakan jalur file atau direktori yang akan diperiksa.
- Mengecek apakah jalur itu sendiri terlarang dengan memanggil fungsi isRestricted.
- Jika jalur itu sendiri tidak terlarang, memeriksa apakah direktori induk dari jalur tersebut terlarang dengan cara menghapus bagian terakhir dari jalur menggunakan strrchr dan memanggil kembali fungsi isRestricted.


```c
static int checkMagic(const char *path)
{
    char fullpath[1000];
    const char *token;
    int flag=0;

    strcpy(fullpath, path);

    // Tokenize the path and check each component
    token = strtok(fullpath, "/");
    const char *lastToken = NULL;
    while (token != NULL)
    {
        lastToken = token;
        token = strtok(NULL, "/");
    }
    
    if(lastToken != NULL && strstr(lastToken, magicWord)!=NULL)
        flag = 1;

    return flag;
}
```
Fungsi ini digunakan untuk memeriksa apakah jalur (path) mengandung kata kunci khusus yang memungkinkan melewati pembatasan akses.
- Menerima argumen path yang merupakan jalur file atau direktori yang akan diperiksa.
- Mengecek apakah jalur (path) mengandung kata kunci khusus yang memungkinkan melewati pembatasan akses. Jika ya, mengembalikan nilai 1 (melewati), jika tidak, mengembalikan nilai 0 (tidak melewati).


```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}
```
Fungsi ini mengimplementasikan operasi getattr dalam FUSE yang mengembalikan informasi atribut file (metadata) seperti mode, ukuran, waktu modifikasi, dan lain-lain.
- Menerima argumen path yang merupakan jalur file atau direktori yang akan diperiksa atributnya, dan stbuf yang akan diisi dengan informasi atribut file.
- Memeriksa apakah jalur tersebut terlarang menggunakan fungsi checkRestricted. Jika terlarang, mengembalikan kode kesalahan ENOENT (file tidak ditemukan).
- Menggunakan fungsi lstat untuk mendapatkan informasi atribut file dan mengisi struktur stbuf.


```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}
```
Fungsi ini mengimplementasikan operasi readdir dalam FUSE yang membaca isi direktori dan mengisi buffer dengan entri direktori.
- Menerima argumen path yang merupakan jalur direktori yang akan dibaca.
- Memeriksa apakah jalur tersebut terlarang menggunakan fungsi checkRestricted. Jika terlarang, mengembalikan kode kesalahan ENOENT (file tidak ditemukan).
- Membuka direktori dengan menggunakan fungsi opendir dan memeriksa apakah direktori berhasil dibuka.
- Membaca entri-entri direktori menggunakan fungsi readdir dan mengisi buffer dengan entri direktori yang ditemukan.


```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}
```
Fungsi ini mengimplementasikan operasi read dalam FUSE yang membaca isi file dan menyalinnya ke buffer.
- Menerima argumen path yang merupakan jalur file yang akan dibaca.
- Memeriksa apakah jalur tersebut terlarang menggunakan fungsi checkRestricted. Jika terlarang, mengembalikan kode kesalahan ENOENT (file tidak ditemukan).
- Membuka file dengan menggunakan fungsi open dan memeriksa apakah file berhasil dibuka.
- Membaca isi file menggunakan fungsi pread dan menyalinnya ke buffer.


```c
static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    // Check if the path or any parent directories are restricted
    if (checkRestricted(fpath))
    {
        appendLog("FAILED", "MKDIR", path,"");
        return -EACCES; // Access denied
    }
    
    int res = mkdir(fpath, mode);

    if (res == -1)
    {
        appendLog("FAILED", "MKDIR", path,"");
        return -errno;
    }
    
    appendLog("SUCCESS", "MKDIR", path,"");
    return 0;
}
```
Fungsi ini mengimplementasikan operasi mkdir dalam FUSE yang membuat direktori baru dengan mode yang ditentukan.
- Menerima argumen path yang merupakan jalur direktori yang akan dibuat.
- Memeriksa apakah jalur tersebut terlarang menggunakan fungsi checkRestricted. Jika terlarang, mengembalikan kode kesalahan ENOENT (file tidak ditemukan).
- Membuat direktori dengan menggunakan fungsi mkdir dan memeriksa apakah direktori berhasil dibuat.


```c
static int xmp_unlink(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Check if the path or any parent directories are restricted
    if (checkRestricted(fpath))
    {
        appendLog("FAILED", "RM", path, "");
        return -EACCES; // Access denied
    }
    
    int res = unlink(fpath);
    if (res == -1)
    {
        appendLog("FAILED", "RM", path, "");
        return -errno;
    }
    
    appendLog("SUCCESS", "RM", path, "");
    return 0;
}
```
Fungsi ini mengimplementasikan operasi unlink dalam FUSE yang menghapus file.
- Menerima argumen path yang merupakan jalur file yang akan dihapus.
- Memeriksa apakah jalur tersebut terlarang menggunakan fungsi checkRestricted. Jika terlarang, mengembalikan kode kesalahan ENOENT (file tidak ditemukan).
- Menghapus file dengan menggunakan fungsi unlink dan memeriksa apakah file berhasil dihapus.


```c
static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Check if the path or any parent directories are restricted
    if (checkRestricted(fpath))
    {
        appendLog("FAILED", "RMDIR", path, "");
        return -EACCES; // Access denied
    }
    
    int res = rmdir(fpath);
    if (res == -1)
    {
        appendLog("FAILED", "RMDIR", path, "");
        return -errno;
    }
    
    appendLog("SUCCESS", "RMDIR", path, "");
    return 0;
}
```
Fungsi ini mengimplementasikan operasi rmdir dalam FUSE yang menghapus direktori.
- Menerima argumen path yang merupakan jalur direktori yang akan dihapus.
- Memeriksa apakah jalur tersebut terlarang menggunakan fungsi checkRestricted. Jika terlarang, mengembalikan kode kesalahan ENOENT (file tidak ditemukan).
- Menghapus direktori dengan menggunakan fungsi rmdir dan memeriksa apakah direktori berhasil dihapus.


```c
static int xmp_rename(const char *oldpath, const char *newpath)
{
    char f_oldpath[1000];
    char f_newpath[1000];
    sprintf(f_oldpath, "%s%s", dirpath, oldpath);
    sprintf(f_newpath, "%s%s", dirpath, newpath);

    // Check if the old path or any parent directories are restricted
    if (checkRestricted(f_oldpath))
         if (!checkMagic(f_newpath))
         {
             appendLog("FAILED", "MV", oldpath, newpath);
             return -EACCES; // Access denied
         }
         
    int res = rename(f_oldpath, f_newpath);
    if (res == -1)
    {
        appendLog("FAILED", "MV", oldpath, newpath);
        return -errno;
    }
    
    appendLog("SUCCESS", "MV", oldpath, newpath);
    return 0;
}
```
Fungsi ini mengimplementasikan operasi rename dalam FUSE yang mengubah nama file atau direktori.
- Menerima argumen oldpath yang merupakan jalur file atau direktori yang akan diubah namanya, dan newpath yang merupakan nama baru.
- Memeriksa apakah jalur tersebut terlarang menggunakan fungsi checkRestricted. Jika terlarang, mengembalikan kode kesalahan ENOENT (file tidak ditemukan).
- Mengganti nama file atau direktori dengan menggunakan fungsi rename dan memeriksa apakah operasi berhasil.


```c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
};
```
Struktur ini berisi pointer ke fungsi yang diimplementasikan untuk setiap operasi yang didukung oleh sistem berkas ini.

```c
int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
Fungsi utama program yang menjalankan FUSE dan menghubungkan operasi-operasi yang diimplementasikan dengan sistem berkas yang akan dijalankan.


- umask(0):
Mengatur umask menjadi 0 untuk memastikan bahwa semua izin file dan direktori yang dibuat memiliki izin penuh.


- return fuse_main(argc, argv, &xmp_oper, NULL):
Menjalankan sistem berkas FUSE dengan menggunakan fungsi fuse_main dan struktur xmp_oper yang berisi implementasi operasi-operasi yang dijalankan pada sistem berkas.

```txt
FAILED::27/05/2023-18:29:39::MKDIR::reo-Create directory /productMagang
FAILED::27/05/2023-18:30:16::MKDIR::reo-Create directory /projectMagang
SUCCESS::27/05/2023-18:30:43::MV::reo-Rename from /restricted_list to /bypass_list
SUCCESS::27/05/2023-18:31:00::MKDIR::reo-Create directory /projectMagang
SUCCESS::27/05/2023-18:31:23::MV::reo-Rename from /filePenting to /restrictedFilePenting
FAILED::27/05/2023-18:31:35::RM::reo-Remove file /ourProject.txt
FAILED::27/05/2023-18:31:51::MV::reo-Rename from /ourProject.txt to /projects.txt
FAILED::27/05/2023-18:31:56::MKDIR::reo-Create directory /newProject
SUCCESS::27/05/2023-18:32:39::MV::reo-Rename from /restrictedFileLama to /bypassrestrictedFileLama
SUCCESS::27/05/2023-18:32:58::MV::reo-Rename from /bypassrestrictedFileLama to /restrictedFileLama_bypass
FAILED::27/05/2023-18:33:08::RMDIR::reo-Remove directory /restrictedFileLama_bypass
SUCCESS::27/05/2023-18:33:39::RM::reo-Remove file /fileLama1.txt
SUCCESS::27/05/2023-18:33:43::RM::reo-Remove file /fileLama2.txt
```
Hasil dari fungsi appenlog() yang mencatat semua kegiatan FUSE yang terjadi

<!-- Langsung drop disini aja penjelasan nya kek biasa, ga perlu pake div div gapapa ehe -->
<!-- Langsung drop disini aja penjelasan nya kek biasa, ga perlu pake div div gapapa ehe -->
<!-- Langsung drop disini aja penjelasan nya kek biasa, ga perlu pake div div gapapa ehe -->

</div>
<br/>
<div align=justify>


## **Soal 3**

Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.
<ol type="a">
<li>

Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah [/etc](https://drive.google.com/file/d/1xBFLFSOsN-DuQCAkKWxyk4GlFH8SprlT/view?usp=share_link) yang bernama <code>secretadmirer.c</code>. 
</li>
<li>
    Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf <code>L, U, T, dan H</code> akan ter-encode dengan <code>Base64</code>.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).
</li>
<li>
    Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan  <code>>file menggunakan lowercase</code> sedangkan untuk <code>direktori semua harus uppercase</code>. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

```c
[direktori] helsa -> HELSA
[file] helsa -› helsa
[nama nya kurang dari atau sama dengan 4 huruf] apt -> 01100001 01110000 01110100
```
</li>
<li>
    Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).
</li>
<li>
    Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam <code>Docker Container</code> menggunakan <code>Docker Compose</code> (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
    <ul>
    <li>
        Container bernama <code>Dhafin</code> akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
    </li>
    <li>
        Container bernama <code>Normal</code> akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 
    </li>
    </ul>
</li>
<li>
    Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image <code>fhinnn/sisop23</code> deh, siapa tahu membantu. 🙂
</li>
</ol>

**Catatan:** 

-   Pastikan dalam FUSE tersebut (baik yang normal dari <code>/etc</code> maupun yang telah dimodifikasi) dapat melakukan operasi <code>mkdir, cat dan mv</code>.
-   Pastikan source mount kalian dari .zip yang udah dikasih (<code>inifolderetc/sisop</code>) dan JANGAN PAKAI <code>/etc</code> ASLI KALIAN! kerusakan Linux bukan tanggung jawab kami.

<br/>

### **Penyelesaian**

Berdasarkan deskripsi pada soal, dapat di ketahui bahwa soal ini meminta kita untuk membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah [/etc](https://drive.google.com/file/d/1xBFLFSOsN-DuQCAkKWxyk4GlFH8SprlT/view?usp=share_link) yang bernama <code>secretadmirer.c</code>.

FUSE akan mengimplementasikan operasi-operasi dasar yang terkait dengan file system, seperti mengakses atribut file, membaca direktori, membaca file, membuat direktori, mengganti nama file/direktori, dan membuka file. Operasi operasi tersebut harus sesuai dengan ketentuan yang ada pada soal.

Berikut adalah struktur program yang digunakan untuk menyelesaikan soal ini.

1.  Direktif Preprocessor:
    -   `#define FUSE_USE_VERSION 28` : Mendefinisikan versi FUSE yang digunakan.
    -   `#include` : Menyertakan package-package yang diperlukan.
2.  Deklarasi Variabel:
    -   `static const char *dirpath` : Menyimpan jalur direktori yang akan digunakan sebagai root dari file sistem yang dibangun menggunakan FUSE atau secara sederhana dapat disebut sebagai `mount point `path``.
3.  Fungsi-fungsi Utilitas:
    -   `base64_encode(const char* input)` : Mengkodekan string input menggunakan Base64.
    -   `toUpper(char* str)` : Mengubah semua karakter dalam string menjadi huruf kapital.
    -   `toLower(char* str)` : Mengubah semua karakter dalam string menjadi huruf kecil.
    -   `getBinaryRepresentation(const char* str)` : Mengembalikan representasi biner dari string input.
    -   `verifyPassword(const char* enteredPassword)`: Memverifikasi kata sandi yang dimasukkan dengan kata sandi yang benar.
4.  Fungsi-fungsi FUSE:
    -   `xmp_getattr(const char *path, struct stat *stbuf)` : Mengambil atribut file/direktori.
    -   `xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)` : Membaca isi direktori.
    -   `xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)` : Membaca isi file.
    -   `xmp_mkdir(const char *path, mode_t mode)` : Membuat direktori.
    -   `xmp_rename(const char *from, const char *to) `: Mengganti nama file/direktori.
    -   `xmp_open(const char *path, struct fuse_file_info *fi)` : Membuka file.
5.  Fungsi `main(int argc, char *argv[])` :
    -   Fungsi utama program, menjalankan file sistem menggunakan fungsi `fuse_main()` dengan operasi-operasi yang didefinisikan dalam struktur `xmp_oper` yang menggunakan fungsi-fungsi di atas.

Ide nya adalah adalah untuk membuat sebuah filesystem khusus yang melakukan enkripsi pada file dan direktori yang ada. FUSE digunakan untuk menghubungkan program ini dengan sistem operasi sehingga program dapat merespons operasi-operasi file seperti membaca, menulis, membuat direktori, dan sebagainya.

Ketika sebuah operasi file dilakukan oleh sistem operasi, FUSE akan memanggil fungsi yang sesuai dalam program ini, seperti `getattr` untuk mendapatkan atribut file, `readdir` untuk membaca isi direktori, `read` untuk membaca isi file, dan lain-lain. Program kemudian akan melakukan enkripsi atau dekripsi terhadap data yang dibaca atau ditulis sesuai dengan aturan yang diberikan dalam kode.

Selain itu, program ini juga menyediakan fungsi `verifyPassword` yang memverifikasi kata sandi sebelum membuka file. Jika kata sandi yang dimasukkan sesuai, file dapat diakses. Jika kata sandi tidak sesuai, file tidak dapat diakses.

Dengan menggunakan konsep ini, program menciptakan sebuah filesystem yang memungkinkan Dhafin untuk menyimpan file dengan enkripsi dan verifikasi kata sandi untuk akses file tersebut.

Berikut adalah pejelasan secara detail mengenai fungsi-fungsi penting yang di gunakan dalam program ini.

<br/>

#### **base64_encode**
```c
static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char* base64_encode(const char* input) {
    size_t input_length = strlen(input);
    size_t output_length = 4 * ((input_length + 2) / 3); // Calculate the output length

    char* encoded_data = (char*)malloc(output_length + 1); // Allocate memory for the encoded data

    if (encoded_data == NULL) {
        printf("Memory allocation failed.\n");
        return NULL;
    }

    size_t i, j;
    for (i = 0, j = 0; i < input_length;) {
        uint32_t octet_a = i < input_length ? (unsigned char)input[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)input[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)input[i++] : 0;

        uint32_t triple = (octet_a << 16) + (octet_b << 8) + octet_c;

        encoded_data[j++] = base64_table[(triple >> 18) & 0x3F];
        encoded_data[j++] = base64_table[(triple >> 12) & 0x3F];
        encoded_data[j++] = base64_table[(triple >> 6) & 0x3F];
        encoded_data[j++] = base64_table[triple & 0x3F];
    }

    // Pad the encoded data if necessary
    size_t padding = input_length % 3;
    for (i = 0; i < padding; i++) {
        encoded_data[output_length - 1 - i] = '=';
    }

    encoded_data[output_length] = '\0'; // Add null terminator

    return encoded_data;
}
```
| **No** | **Syntax** | **Keterangan** |
| ------------- | ------------- | ------------- |
| 1 | `static const char base64_table[]` | Array karakter yang berisi karakter-karakter yang digunakan dalam enkripsi base64. Setiap karakter pada array ini akan digunakan untuk mengonversi nilai numerik ke karakter base64 yang sesuai. |
| 2 | `char* base64_encode(const char* input)` | Fungsi ini menerima input string yang akan dienkripsi menjadi base64 dan mengembalikan string hasil enkripsi base64. |
| 3 | `size_t input_length = strlen(input)` | Menghitung panjang string input. |
| 4 | `size_t output_length = 4 * ((input_length + 2) / 3)` | Menghitung panjang string output yang akan digunakan untuk menyimpan hasil enkripsi base64. Setiap 3 byte data input akan dienkripsi menjadi 4 karakter base64. Jika panjang input tidak habis dibagi 3, maka akan dilakukan padding dengan karakter '='. |
| 5 | `char* encoded_data = (char*)malloc(output_length + 1)` | Mengalokasikan memori untuk menyimpan string hasil enkripsi base64. Jumlah byte yang dialokasikan adalah output_length + 1 untuk null terminator. |
| 6 | `if (encoded_data == NULL)` | Memeriksa apakah alokasi memori berhasil. Jika gagal, maka akan ditampilkan pesan error dan fungsi akan mengembalikan nilai NULL.L. |
| 7 | `uint32_t octet_a = i < input_length ? (unsigned char)input[i++] : 0` | Mengambil karakter pertama dari input dan mengonversinya ke nilai numerik (dalam bentuk uint32_t). Jika sudah mencapai akhir input, nilai defaultnya adalah 0. |
| 8 | `uint32_t triple = (octet_a << 16) + (octet_b << 8) + octet_c` | Menggabungkan tiga karakter input menjadi nilai numerik 24-bit yang akan dienkripsi menjadi 4 karakter base64. |
| 9 | `encoded_data[j++] = base64_table[(triple >> 18) & 0x3F]` |   Menyimpan karakter base64 pertama ke dalam string output dengan menggunakan nilai numerik yang dihasilkan dari langkah sebelumnya dan operasi bitwise. |
| 10 | `size_t padding = input_length % 3` | Menghitung jumlah karakter padding yang akan ditambahkan pada string output. |
| 11 | `for (i = 0; i < padding; i++)` | Melakukan perulangan sebanyak padding untuk menambahkan karakter '=' pada string output. |
| 12 | `encoded_data[output_length - 1 - i] = '='` | Menambahkan karakter '=' pada string output. |
| 13 | `encoded_data[output_length] = '\0'` | Menambahkan null terminator pada string output. |
| 14 | `return encoded_data` | Mengembalikan string output. |

Ide dari fungsi ini adalah melakukan enkripsi pada setiap tiga karakter input menjadi empat karakter `base64`. Fungsi ini kemudian menggunakan tabel karakter `base64` yang ditentukan dalam `base64_table` untuk melakukan pengonversian. Proses enkripsi dilakukan dengan menggunakan operasi bitwise dan shifting

Harapannya adalah fungsi ini dapat menutupi rahasia dhafin dan semua directory dan file yang berada pada direktori yang diawali dengan huruf `L, U, T, dan H` akan ter-encode dengan `Base64`.  Dan juga encode akan berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat)

<br/>

#### **toLower** dan **toUpper**
```c
char* toUpper(char* str) {
    int i = 0;
    while (str[i]) {
        str[i] = toupper(str[i]);
        i++;
    }
    return str;
}

char* toLower(char* str) {
    int i = 0;
    while (str[i]) {
        str[i] = tolower(str[i]);
        i++;
    }
    return str;
}
```
Fungsi `toUpper` menerima sebuah string sebagai argumen dan mengubah setiap karakter dalam string tersebut menjadi huruf kapital. Prosesnya dilakukan dengan menggunakan fungsi `toupper`, yang merupakan bagian dari library `<ctype.h>`. Fungsi ini akan berjalan dalam sebuah loop while yang akan berhenti ketika mencapai karakter null ('\0') yang menandakan akhir dari string. Setiap karakter dalam string akan diganti dengan versi huruf kapitalnya menggunakan fungsi `toupper`. Setelah semua karakter dalam string diubah, fungsi akan mengembalikan pointer ke string yang telah diubah.

Fungsi `toLower` memiliki konsep yang mirip dengan `toUpper`, namun kali ini semua karakter dalam string akan diubah menjadi huruf kecil. Prosesnya juga menggunakan fungsi `tolower` yang juga merupakan bagian dari library `<ctype.h>`.

<br/>

#### **getBinnerRepresentation**
```c
char* getBinaryRepresentation(const char* str) {
    int length = strlen(str);
    char* binaryStr = (char*)malloc((8 * length + 1) * sizeof(char)); // Allocate memory for binary string

    if (binaryStr == NULL) {
        printf("Memory allocation failed.\n");
        return NULL;
    }

    int index = 0;
    for (int i = 0; i < length; i++) {
        unsigned char ch = (unsigned char)str[i]; // Cast to unsigned char to ensure consistent behavior
        for (int j = 7; j >= 0; j--) {
            binaryStr[index++] = ((ch >> j) & 1) + '0'; // Convert bit to character '0' or '1'
        }
        binaryStr[index++] = ' ';
    }
    binaryStr[index - 1] = '\0'; // Replace the last space with null character

    return binaryStr;
}
```
fungsi `getBinaryRepresentation` memiliki tujuan untuk mengonversi string menjadi representasi biner. Berikut adalah penjelasan singkat nya:

1.  Menghitung panjang string menggunakan `strlen(str)` dan menyimpannya dalam length.
2.   Mengalokasikan memori untuk string biner menggunakan `malloc` dengan ukuran `(8 * length + 1) * sizeof(char)`.
3.  Melakukan iterasi melalui setiap karakter dalam string `str`:
    -   Mengkonversi karakter ke tipe unsigned char dan menyimpannya dalam `ch`.
    -   Menggunakan loop `for` untuk mengambil setiap bit dalam `ch` dan mengonversinya menjadi karakter `'0' atau '1'`.
    -   Memasukkan karakter hasil konversi ke dalam string biner.
    -   Menambahkan spasi setelah setiap byte dalam string biner.
4.  Mengganti spasi terakhir dalam string biner dengan karakter null `('\0') `sebagai penanda akhir string.
5.  Mengembalikan pointer ke string biner yang dihasilkan.

Dengan menggunakan metode ini, string masukan diiterasi karakter per karakter dan setiap bit-nya diambil secara efisien. Memori juga dialokasikan dengan mempertimbangkan ukuran yang dibutuhkan

<br/>

#### **verifyPassword**
```c
static bool verifyPassword(const char* enteredPassword) {
    const char* correctPassword = "0311";
    return strcmp(enteredPassword, correctPassword) == 0;
}
```
Fungsi `verifyPassword` membandingkan kata sandi yang dimasukkan dengan kata sandi yang benar. Jika kedua kata sandi tersebut sama, fungsi mengembalikan `true`; jika tidak, fungsi mengembalikan `false`. Fungsi ini akan di panggil pada fungsi fuse `xmp_open` sehingga ketika file di buka, maka akan di cek terlebih dahulu apakah password yang dimasukkan benar atau salah.

<br/>

#### **xmp_readdir**

<details>
    <summary>Click to show the code! </summary>

```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    //printf("%s\n",fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
    
        char f[1000], base[1000], opath[1000], npath[1000];
        strcpy(f,de->d_name);
        
        if(strlen(f) <= 4 )
        {
            strcpy(base,getBinaryRepresentation(f));
            //printf("bin - %s -> %s\n", f, base);
        }
        else if(de->d_type == DT_DIR && (f[0] != 0 && f[0] != 1))
        {
            strcpy(base,toUpper(f));
            //printf("dir - %s -> %s\n", f, base);
        }
        else if(de->d_type == DT_REG && (f[0] != 0 && f[0] != 1))
        {
            strcpy(base,toLower(f));
            //printf("file - %s -> %s\n", f, base);
        }
        else
            strcpy(base,f);
        
        strcpy(opath,fpath);
    	strcpy(npath,fpath);
    	strcat(opath,"/");
    	strcat(npath,"/");
    	strcat(opath,de->d_name);
    	strcat(npath,base);
    	
        
    	if ((de->d_type == DT_REG) && (
    	     f[0] == 'L' || f[0] == 'U' || f[0] == 'T' || f[0] == 'H' ||
             f[0] == 'l' || f[0] == 'u' || f[0] == 't' || f[0] == 'h'))
    	{
    	     // Open the file for reading
            FILE* file = fopen(opath, "rb");
            if (file != NULL) {
                // Determine the file size
                fseek(file, 0, SEEK_END);
                long file_size = ftell(file);
                fseek(file, 0, SEEK_SET);

                // Allocate memory for file content
                char* file_content = (char*)malloc(file_size);
                if (file_content != NULL) {
                    // Read the file content
                    fread(file_content, 1, file_size, file);
                    
                    if(strchr(file_content, '!') == NULL){
		             // Encode the file content with base64
		            char* encoded_content = base64_encode(file_content);
		            strcat(encoded_content,"\n!");
		            if (encoded_content != NULL) {
		                // Create a new file with the encoded content
		                FILE* encoded_file = fopen(npath, "wb");
		                if (encoded_file != NULL) {
		                    // Write the encoded content to the file
		                    fwrite(encoded_content, 1, strlen(encoded_content), encoded_file);
		                    fclose(encoded_file);
		                } else {
		                    printf("Failed to create the encoded file: %s\n", npath);
		                }
		                
		                free(encoded_content);
		            } else {
		                printf("Failed to encode file content: %s\n", opath);
		            }
                    }

                    free(file_content);
                } else {
                    printf("Memory allocation failed.\n");
                }
                
                fclose(file);
            } else {
                printf("Failed to open file: %s\n", opath);
            }
    	}
    	
    	rename(opath,npath);
    	
        struct stat st;
	
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}
```
</details>

Fungsi `xmp_readdir` digunakan untuk membaca isi dari direktori path yang telah di tentukan. Fungsi ini menerima beberapa argumen, yaitu `path` (path direktori yang akan dibaca), `buf` (buffer untuk mengisi entri direktori), `filler` (fungsi untuk mengisi entri direktori ke buffer), `offset` (offset file), dan `fi` (informasi file).

Pada awal fungsi, `fpath` diinisialisasi sebagai `path` lengkap dari direktori yang akan dibaca. Jika `path` adalah "/", maka `path` akan diganti dengan `dirpath` (direktori yang ditentukan sebelumnya) dan `fpath` akan diisi dengan `path`. Jika `path` bukan "/", maka `fpath` akan diisi dengan gabungan dari `dirpath` dan `path`.

Selanjutnya, fungsi membuka direktori yang ditentukan menggunakan `opendir`. Jika `dp` (direktori) sama dengan `NULL`, artinya direktori tidak dapat dibuka, maka fungsi mengembalikan `-errno` (nilai negatif yang menunjukkan kesalahan).

Selama ada entri direktori yang dibaca menggunakan `readdir`, fungsi akan memproses entri tersebut. Pertama, nama entri direktori disalin ke variabel `f`. Kemudian, terdapat beberapa kondisi yang diperiksa:

-   Jika panjang nama file kurang dari atau sama dengan 4 karakter, `base` akan diisi dengan representasi biner dari `f` menggunakan fungsi getBinaryRepresentation.
-   Jika jenis entri direktori adalah direktori (`DT_DIR`) dan karakter pertama dari `f` bukan 0 atau 1, `base` akan diisi dengan `f` yang dikonversi menjadi huruf kapital menggunakan fungsi `toUpper`.
-   Jika jenis entri direktori adalah file (`DT_REG`) dan karakter pertama dari `f` bukan 0 atau 1, `base` akan diisi dengan f yang dikonversi menjadi huruf kecil menggunakan fungsi `toLower`.
-   Jika tidak memenuhi kondisi di atas, `base` akan diisi dengan f tanpa perubahan.

Selanjutnya, `opath` dan npath diinisialisasi sebagai `path` lengkap dari file asli (`fpath/de->d_name`) dan `path` lengkap dari file yang akan diubah namanya `(fpath/base).` Jika jenis entri direktori adalah file (`DT_REG`) dan karakter pertama dari `f` adalah `'L', 'U', 'T', 'H', 'l', 'u', 't', atau 'h'`, maka file akan dibuka untuk dibaca (`fopen`). Jika file berhasil dibuka, ukuran file ditentukan dan memori dialokasikan untuk menyimpan kontennya.

Selanjutnya, jika konten file tidak mengandung karakter `'!'`, konten file akan `dienkripsi` menggunakan fungsi `base64_encode` dan ditambahkan karakter '\n' dan '!'. Hasil enkripsi akan ditulis ke file yang baru dibuat (`encoded_file`). Jika file berhasil dibuat dan ditulis, maka file asli akan diubah namanya menjadi `base`.

Setelah itu, struktur `st` (informasi file) diinisialisasi dengan menggunakan informasi dari entri direktori (`de`). Informasi tersebut meliputi nomor inode (`d_ino`) dan mode file (`d_type`) yang dikonversi menjadi mode direktori (<< `12`).

Selanjutnya, isi dari entri direktori ditambahkan ke `buffer` menggunakan fungsi `filler`. Jika `filler` mengembalikan nilai bukan 0, berarti buffer penuh dan proses pembacaan direktori dihentikan.

Terakhir, direktori yang dibuka ditutup dengan menggunakan `closedir`.

Fungsi `xmp_readdir` mengembalikan nilai 0 sebagai tanda bahwa proses membaca direktori telah selesai.

<br/>

### **xmp_read**
<details>
<summary>Clik here to show the code!</summary>

```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}
```
</details>

Fungsi `xmp_read` digunakan untuk membaca isi dari sebuah file. Fungsi `xmp_read` mengembalikan nilai res sebagai tanda berhasil atau tidaknya proses pembacaan file.

<br/>

#### **xmp_mkdir**
<details>
<summary>Clik here to show the code!</summary>

```c
static int xmp_mkdir(const char *path, mode_t mode) {
    char fpath[1000], npath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
        
    // Check if the directory name starts with L, U, T, or H
    char f[1000], base[1000];
    strcpy(f,strrchr(path, '/') + 1);
    
    if(strlen(f) <= 4)
        strcpy(base,getBinaryRepresentation(f));
    else
        strcpy(base,toUpper(f));
    
    strcpy(npath,fpath);
    char *lastSlash = strrchr(npath, '/')+1;
    if (lastSlash != NULL) 
        *lastSlash = '\0';
    strcat(npath,base); 
    
    char *lastSlashOr = strrchr(path, '/')+1;
    if (lastSlashOr != NULL) 
        *lastSlashOr = '\0';
    strcat(path,base);
    
    int res = mkdir(npath, mode); 
    
    if (res == -1)
        return -errno;
        
    return 0;
}
```
</details>

Fungsi `xmp_mkdir` digunakan untuk membuat direktori baru. Fungsi ini menerima dua argumen, yaitu `path` (path direktori yang akan dibuat) dan `mode` (mode permission untuk direktori).

Pada awal fungsi, `fpath` diinisialisasi sebagai path lengkap dari direktori yang akan dibuat dengan menggabungkan `dirpath` dan `path`.

Selanjutnya, fungsi melakukan beberapa manipulasi pada `path` untuk memperoleh nama direktori yang akan digunakan sebagai nama baru. Nama direktori diambil dari `path` dengan menghapus semua karakter sebelum tanda / terakhir menggunakan `strrchr`. Jika panjang nama direktori kurang dari atau sama dengan 4 karakter, nama direktori akan diubah menjadi `representasi biner` dengan menggunakan fungsi `getBinaryRepresentation`. Jika panjang nama direktori lebih dari 4 karakter, nama direktori akan diubah menjadi huruf kapital menggunakan fungsi `toUpper`. Hasil perubahan nama direktori disimpan dalam `base`.

Kemudian, `npath` diinisialisasi sebagai path lengkap baru untuk direktori yang akan dibuat. `npath` diisi dengan `fpath`, dan karakter setelah tanda `/` terakhir dalam `npath` diubah menjadi `null` character. Setelah itu, base ditambahkan ke `npath` untuk membentuk path lengkap baru.

Selanjutnya, karakter setelah tanda `/` terakhir dalam path diubah menjadi `null` character, dan base ditambahkan ke path untuk mengganti nama direktori dalam path.

Selanjutnya, fungsi menggunakan mkdir untuk membuat direktori dengan path `npath` dan mode permission mode. Jika `res` (nilai yang dikembalikan oleh mkdir) sama dengan -1, artinya terjadi kesalahan dalam pembuatan direktori, maka fungsi mengembalikan `-errno` (nilai negatif yang menunjukkan kesalahan).

<br/>

#### **xmp_rename**
<details>
<summary>Clik here to show the code!</summary>

```c
static int xmp_rename(const char *from, const char *to) {

    char fpath_from[1000];
    char fpath_to[1000];
    char npath[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);
    
    struct stat st;
    int rest = lstat(fpath_from, &st);
    if (rest == -1)
        return -errno;
    
    char f[1000], base[1000];
    strcpy(f,strrchr(to, '/') + 1);
    //printf("%s\n",f);
    
    if(strlen(f) <= 4)
        //printf("bin\n");
        strcpy(base,getBinaryRepresentation(f));
    else if(S_ISDIR(st.st_mode))
        //printf("dir\n");
        strcpy(base,toUpper(f));
    else
        //printf("file\n");
        strcpy(base,toLower(f));
        
    strcpy(npath,fpath_from);
    char *lastSlash = strrchr(npath, '/')+1;
    if (lastSlash != NULL) 
        *lastSlash = '\0';
    strcat(npath,base); 
    
    char *lastSlashOr = strrchr(to, '/')+1;
    if (lastSlashOr != NULL) 
        *lastSlashOr = '\0';
    strcat(to,base);
    
    //printf("%s\n",base);
 
    int res = rename(fpath_from, npath);
    
    if (res == -1)
        return -errno;

    return 0;
}
```
</details>

Fungsi `xmp_rename` digunakan untuk melakukan pengubahan nama atau pemindahan file atau direktori. fungsi melakukan beberapa manipulasi pada `to` untuk memperoleh nama baru yang akan digunakan. Nama baru diambil dari `to` dengan menghapus semua karakter sebelum tanda / terakhir menggunakan `strrchr`. Jika panjang nama baru kurang dari atau sama dengan 4 karakter, nama baru akan diubah menjadi representasi biner dengan menggunakan fungsi `getBinaryRepresentation`. Jika `from` adalah direktori, nama baru akan diubah menjadi huruf kapital menggunakan fungsi `toUpper`. Jika `from` adalah file, nama baru akan diubah menjadi huruf kecil menggunakan fungsi `toLower`. Hasil perubahan nama baru disimpan dalam base.

Kemudian, `npath` diinisialisasi sebagai path lengkap baru yang akan digunakan. `npath` diisi dengan fpath_from, dan karakter setelah tanda / terakhir dalam `npath` diubah menjadi null character. Setelah itu, `base` ditambahkan ke `npath` untuk membentuk path lengkap baru.

Selanjutnya, karakter setelah tanda / terakhir dalam to diubah menjadi null character, dan `base` ditambahkan ke to untuk mengganti nama file atau direktori dalam path.

Selanjutnya, fungsi menggunakan rename untuk melakukan pengubahan nama atau pemindahan file atau direktori dari `fpath_from` ke `npath`. Jika `res` (nilai yang dikembalikan oleh rename) sama dengan -1, artinya terjadi kesalahan dalam proses pengubahan nama atau pemindahan, maka fungsi mengembalikan `-errno` (nilai negatif yang menunjukkan kesalahan).

<br/>

#### **xmp_open**
<details>
<summary>Clik here to show the code!</summary>

```c
static int xmp_open(const char *path, struct fuse_file_info *fi) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    //printf("%s\n",path);

    //Prompt for password
    char password[100];
    printf("Enter password: ");
    scanf("%s", password);

    if (!verifyPassword(password)) {
        printf("Incorrect password. Access denied.\n");
        return -EACCES; // Return appropriate error code for access denied
    }

    int fd = open(fpath, fi->flags);
    if (fd == -1)
        return -errno;

    fi->fh = fd;
    return 0;
}
```
</details>

Fungsi `xmp_open` digunakan untuk membuka file. fungsi meminta pengguna memasukkan `password` dengan menggunakan `printf` untuk menampilkan pesan dan scanf untuk membaca `input` password dari pengguna.

Setelah itu, fungsi memanggil fungsi `verifyPassword` untuk memverifikasi password yang dimasukkan oleh pengguna. Jika password tidak valid, fungsi mencetak pesan "Incorrect password. Access denied." dan mengembalikan` -EACCES` (kode kesalahan yang menunjukkan akses ditolak).

Jika password valid, fungsi melanjutkan dengan memanggil `open` untuk membuka file dengan menggunakan `fpath` dan `fi->flags`. Jika open mengembalikan nilai -1, artinya terjadi kesalahan dalam membuka file, maka fungsi mengembalikan `-errno` (nilai negatif yang menunjukkan kesalahan).

Jika pembukaan file berhasil, file descriptor (fd) disimpan dalam `fi->fh (file handle)` untuk digunakan pada operasi file selanjutnya. Terakhir, fungsi mengembalikan nilai 0 sebagai tanda berhasilnya proses pembukaan file.

#### **Docker Compose**
Berikut adalah konfigurasi Docker Compose yang mencakup dua kontainer, yaitu Dhafin dan Normal, untuk melakukan mounting FUSE yang telah dimodifikasi dan mounting normal tanpa modifikasi:

```yml
version: '3'
services:
  normal-container:
    image: ubuntu:bionic
    container_name: Normal
    tty: true
    volumes:
      - ./inifolderetc/sisop:/fuse_mounte
  dhafin-container:
    image: ubuntu:bionic
    container_name: Dhafin
    tty: true
    volumes:
      - ./etc:/fuse_mounte
```
Pada konfigurasi di atas, terdapat dua service, dhafin dan normal, yang akan dibangun menggunakan Dockerfile yang sama. Perbedaannya terletak pada volume yang digunakan.

Pada service `normal`, volume yang digunakan adalah `./inifolderetc/sisop:/fuse_mounte`. Volume ini akan digunakan untuk melakukan mounting normal tanpa modifikasi. Pada service `dhafin`, volume yang digunakan adalah `./etc:/fuse_mounte`. Volume ini akan digunakan untuk melakukan mounting FUSE yang telah dimodifikasi.

Selanjutnya, docker compose dapat dijalankan dengan menggunakan perintah `docker-compose up -d`. Setelah itu, dapat dilakukan pengecekan dengan menggunakan perintah `docker ps` untuk melihat apakah kedua kontainer telah berjalan. Selain itu kita dapat mengecek apakah kedua kontainer telah melakukan mounting dengan benar, dengan menggunakan perintah `docker exec -it <container_name> ls /fuse_mounte`. Jika kedua kontainer telah melakukan mounting dengan benar, maka pada service `normal` akan menampilkan isi dari folder `/inifolderetc/sisop` dan pada service `dhafin` akan menampilkan isi dari folder `/etc` yang telah dimodifikasi.
</div>
